import EmberRouter from '@ember/routing/router';
import config from 'ember-quickstart/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('authors', function () {
    this.route('edit', { path: 'edit/:author_id' });
    this.route('add');
  });
  this.route('books', { path: 'books/:author_id' }, function () {
    this.route('edit', { path: 'edit/:book_id' });
    this.route('add');
  });
});
