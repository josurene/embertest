import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class BooksRoute extends Route {
  @service store;
  model(params) {
    let { author_id } = params;
    if (Number.isInteger(author_id)) {
      return this.store.findRecord('author', author_id, {
        include: 'books'
      });
    }
    return {
      name: author_id,
      books: this.store.findAll('book'),
    };
  }
}
