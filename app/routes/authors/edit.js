import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class AuthorsEditRoute extends Route {
  @service store;
  model(params) {
    let { author_id } = params;
    const author = this.store.findRecord('author', author_id);
    this.name = author.name;
    return author;
  }
}
