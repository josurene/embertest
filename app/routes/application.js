import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import {getUserPhoto} from "../common/functions";

export default class ApplicationRoute extends Route {
  @service store;
  model() {
    let settings = this.store.peekRecord('settings', 1);
    // Just for testing purposes since there is no api
    if (!settings?.notFirstTime) {
      this.initSetup();
    }
  }

  /**
   * Creates authors and books for a real simulation
   * @returns {Promise<void>}
   */
  async initSetup() {

    this.store.createRecord('author', {
      id: 1,
      name: 'Stephen king',
      photo: await getUserPhoto(),
    }).save();

    this.store.createRecord('author', {
      id: 2,
      name: 'Jun Eishima',
      photo: await getUserPhoto(),
    }).save();

    this.store.createRecord('author', {
      id: 3,
      name: 'Nissiosin',
      photo: await getUserPhoto(),
    }).save();

    let stephen = this.store.peekRecord('author', 1);
    this.store.createRecord('book', {
      id: 1,
      author: stephen,
      name: 'It',
    }).save();
    this.store.createRecord('book', {
      id: 2,
      author: stephen,
      name: 'Kujo',
    }).save();
    this.store.createRecord('book', {
      id: 3,
      author: stephen,
      name: 'Sack of bones',
    }).save();

    let jun = this.store.peekRecord('author', 2);
    this.store.createRecord('book', {
      id: 4,
      author: jun,
      name: 'Long Story Short',
    }).save();
    this.store.createRecord('book', {
      id: 5,
      author: jun,
      name: 'Short Story Long',
    }).save();


    let nissiosin = this.store.peekRecord('author', 3);
    this.store.createRecord('book', {
      id: 6,
      author: nissiosin,
      name: 'Bakemonogatari',
    }).save();
    this.store.createRecord('book', {
      id: 7,
      author: nissiosin,
      name: 'Nisemonogatari',
    }).save();
    this.store.createRecord('book', {
      id: 8,
      author: nissiosin,
      name: 'Kizumonogatari',
    }).save();
    this.store.createRecord('book', {
      id: 9,
      author: nissiosin,
      name: 'Nekomonogatari',
    }).save();

    this.store.createRecord('settings', {
      id: 1,
      notFirstTime: true,
    }).save();
  }
}
