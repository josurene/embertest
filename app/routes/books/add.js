import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class BooksAddRoute extends Route {
  @service store;
  model() {
    return {
      authors: this.store.findAll('author')
    };
  }
}
