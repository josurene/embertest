import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';

export default class AuthorsController extends Controller {
  queryParams = ['name'];

  @tracked name = null;

  @tracked model;

  /**
   * Filters authors by name
   * @returns {*}
   */
  get filteredAuthors() {
    let name = this.name;
    let authors = this.model;

    if (name) {
      return authors.filter((author) => {
        return author.name.toLowerCase().includes(name.toLowerCase());
      });
    } else {
      return authors;
    }
  }


}
