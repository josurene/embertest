import Controller from '@ember/controller';
import {tracked} from "@glimmer/tracking";

export default class BooksController extends Controller {
  queryParams = ['name'];

  @tracked name = null;

  @tracked model;

  /**
   * Filters Books by name
   * @returns {*}
   */
  get filteredBooks() {
    let name = this.name;
    let { books } = this.model;

    if (name) {
      return books.filter((book) => {
        return book.name.toLowerCase().includes(name.toLowerCase());
      });
    } else {
      return books;
    }
  }
}
