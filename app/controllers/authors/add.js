import Controller from '@ember/controller';
import {getUserPhoto} from "../../common/functions";
import { action } from '@ember/object';

export default class AuthorsAddController extends Controller {

  /**
   * Creates a new author entry
   * @returns {Promise<void>}
   */
  @action
  async submit() {
    this.store.createRecord('author', {
      name: this.name,
      photo: await getUserPhoto(),
    }).save();
    this.transitionToRoute('authors');
  }
}
