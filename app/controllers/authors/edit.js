import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class AuthorsEditController extends Controller {
  @service store;

  /**
   * Saves an author edit
   * @param author
   */
  @action
  submit(author) {
    author.name = this.name;
    author.save();
    this.transitionToRoute('authors');
  }
}
