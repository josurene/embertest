import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class BooksEditController extends Controller {
  @service store;

  /**
   * Saves an edited book
   * @param book
   */
  @action
  submit(book) {
    book.name = this.name;
    book.author = this.author;
    book.save();
    this.transitionToRoute('books');
  }
}
