import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class BooksAddController extends Controller {

  /**
   * Creates a new book entry
   * @returns {Promise<void>}
   */
  @action
  async submit() {
    this.store.createRecord('book', {
      name: this.name,
      author: this.author,
    }).save();
    this.transitionToRoute('books');
  }
}
