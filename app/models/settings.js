import Model, { attr } from '@ember-data/model';

export default class SettingsModel extends Model {
  @attr('boolean') notFirstTime;
}
