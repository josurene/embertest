import Model, { attr, belongsTo } from '@ember-data/model';

export default class BookModel extends Model {
  @belongsTo('author') author;
  @attr('string') name;
}
