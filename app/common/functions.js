import fetch from 'fetch';

const getUserPhoto = async () => {
  const response = await fetch('https://randomuser.me/api/');
  let { results } = await response.json();

  return results[0].picture.medium;
}

export {
  getUserPhoto,
}
