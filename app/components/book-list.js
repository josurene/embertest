import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class BookListComponent extends Component {
  @service modalsManager;

  /**
   * Deletes the selected book after launching a modal
   * @param book
   */
  @action
  deleteConfirm(book) {
    this.modalsManager
      .confirm({title: 'Delete ' + book.name, body: 'Are you sure'})
      .then(() => {
        book.destroyRecord();
      })
      .catch(() => {
      });
  }
}
