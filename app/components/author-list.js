import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class PeopleListComponent extends Component {
  @service modalsManager;

  /**
   * Launches a modal and lets the user decide if they want to delete the author
   * @param author
   */
  @action
  deleteConfirm(author) {
    this.modalsManager
      .confirm({title: 'Delete ' + author.name, body: 'Are you sure'})
      .then(() => {
        author.destroyRecord();
      })
      .catch(() => {
      });
  }
}
